import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
const apiUrl = 'http://localhost:3000/apiNotif';
let ApiNotifService = class ApiNotifService {
    constructor(http) {
        this.http = http;
    }
    handleError(operation = 'operation', result) {
        return (error) => {
            console.error(error);
            return of(result);
        };
    }
    //GET
    getNotifications() {
        return this.http.get(`${apiUrl}`, httpOptions)
            .pipe(catchError(this.handleError('getNotifications', [])));
    }
    getNotificationById(id) {
        const url = `${apiUrl}/${id}`;
        return this.http.get(url).pipe(catchError(this.handleError(`getNotificationById=${id}`)));
    }
    //POST
    addNotification(notification) {
        return this.http.post(apiUrl, notification, httpOptions).pipe(catchError(this.handleError('addNotifications')));
    }
    //PUT
    updateNotification(id, notification) {
        const url = `${apiUrl}/${id}`;
        return this.http.put(url, notification, httpOptions).pipe(catchError(this.handleError('updateNotification')));
    }
    //DELETE
    deleteNotificiation(id) {
        const url = `${apiUrl}/${id}`;
        return this.http.delete(url, httpOptions)
            .pipe(catchError(this.handleError('deleteNotification')));
    }
};
ApiNotifService = __decorate([
    Injectable({
        providedIn: 'root'
    })
], ApiNotifService);
export { ApiNotifService };
//# sourceMappingURL=api-notif.service.js.map