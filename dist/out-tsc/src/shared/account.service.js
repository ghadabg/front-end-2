import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { of } from 'rxjs';
import { catchError } from 'rxjs/operators';
const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
const REST_API_SERVER = 'http://localhost:3000/account/';
let AccountService = class AccountService {
    constructor(http) {
        this.http = http;
    }
    handleError(operation = 'operation', result) {
        return (error) => {
            console.error(error);
            return of(result);
        };
    }
    getAccounts() {
        return this.http.get(REST_API_SERVER, httpOptions)
            .pipe(catchError(this.handleError('getAccounts', [])));
    }
    getAccountsbyNo(no) {
        const url = REST_API_SERVER + no;
        return this.http.get(url, httpOptions)
            .pipe(catchError(this.handleError('GetAccountByNo', [])));
    }
    getAccountsbyClient(cl) {
        const url = REST_API_SERVER + 'client/' + cl;
        return this.http.get(url, httpOptions)
            .pipe(catchError(this.handleError('GetAccountByClient', [])));
    }
};
AccountService = __decorate([
    Injectable({
        providedIn: 'root'
    })
], AccountService);
export { AccountService };
//# sourceMappingURL=account.service.js.map