import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { of } from 'rxjs';
import { catchError } from 'rxjs/operators';
const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
const REST_API_SERVER = 'http://localhost:3000/apiclient/';
let ApiClientService = class ApiClientService {
    constructor(http) {
        this.http = http;
    }
    handleError(operation = 'operation', result) {
        return (error) => {
            console.error(error);
            return of(result);
        };
    }
    //get ALL Clients
    getClients() {
        return this.http.get(REST_API_SERVER, httpOptions)
            .pipe(catchError(this.handleError('getClients', [])));
    }
    //get Client by id
    getCLientById(id) {
        const url = REST_API_SERVER + 'id/' + id;
        return this.http.get(url).pipe(catchError(this.handleError(`getClientById=${id}`)));
    }
    //get Client by Cin
    getCLientByCIN(cin) {
        const url = REST_API_SERVER + 'CIN/' + cin;
        return this.http.get(url).pipe(catchError(this.handleError(`getClientByCIN=${cin}`)));
    }
    //get Client by CUSTOMER
    getCLientByCustomer(cl) {
        const url = REST_API_SERVER + 'client/' + cl;
        return this.http.get(url).pipe(catchError(this.handleError(`getClientByCIN=${cl}`)));
    }
    //get Client Name
    getCLientByName(cl) {
        const url = REST_API_SERVER + 'Nom/' + cl;
        return this.http.get(url).pipe(catchError(this.handleError(`getClientByCIN=${cl}`)));
    }
    //get Client Mail
    getCLientByMail(cl) {
        const url = REST_API_SERVER + 'Mail/' + cl;
        return this.http.get(url).pipe(catchError(this.handleError(`getClientMail=${cl}`)));
    }
    addClient(client) {
        return this.http.post(REST_API_SERVER, client, httpOptions).pipe(catchError(this.handleError('addClient')));
    }
    updateClient(id, client) {
        const url = REST_API_SERVER + '/' + id;
        return this.http.put(url, client, httpOptions).pipe(catchError(this.handleError('updateCLient')));
    }
    deleteClient(id) {
        const url = REST_API_SERVER + '/' + id;
        return this.http.delete(url, httpOptions)
            .pipe(catchError(this.handleError('deleteCLient')));
    }
};
ApiClientService = __decorate([
    Injectable({
        providedIn: 'root'
    })
], ApiClientService);
export { ApiClientService };
//# sourceMappingURL=apiclient.service.js.map