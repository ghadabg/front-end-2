export function passwordvalidator(control) {
    const password = control.get('password');
    const confirmPassword = control.get('confirmPassword');
    if (password.pristine || confirmPassword.pristine) {
        return null;
    }
    return password && confirmPassword && password.value !== confirmPassword.value ? { 'misMatch': true } : null;
}
//# sourceMappingURL=passwordvalidator.service.js.map