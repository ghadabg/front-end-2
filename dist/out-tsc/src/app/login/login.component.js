import { __decorate } from "tslib";
import { Component } from '@angular/core';
import { Validators } from '@angular/forms';
let LoginComponent = class LoginComponent {
    constructor(router, formBuilder) {
        this.router = router;
        this.formBuilder = formBuilder;
        this.submitted = false;
        this.isCorrect = false;
    }
    ngOnInit() {
        this.LoginForm = this.formBuilder.group({
            email: ['', [Validators.required, Validators.email]],
            password: ['', Validators.required]
        });
    }
    onSubmit() {
        this.submitted = true;
        // stop here if form is invalid
        if (this.LoginForm.invalid) {
            this.isCorrect = true;
        }
        // display form values on success
        alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.LoginForm.value, null, 4));
    }
    get f() { return this.LoginForm.controls; }
    navigateToRegister() {
        this.router.navigate(['register']);
        console.log("Success Navigation");
    }
    navigateToAdmin() {
        this.router.navigate(['admin']);
    }
};
LoginComponent = __decorate([
    Component({
        selector: 'app-login',
        templateUrl: './login.component.html',
        styleUrls: ['./login.component.css']
    })
], LoginComponent);
export { LoginComponent };
//# sourceMappingURL=login.component.js.map