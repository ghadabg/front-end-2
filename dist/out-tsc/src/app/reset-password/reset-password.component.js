import { __decorate } from "tslib";
import { Component, Output } from '@angular/core';
import { Validators } from '@angular/forms';
let ResetPasswordComponent = class ResetPasswordComponent {
    constructor(router, formBuilder) {
        this.router = router;
        this.formBuilder = formBuilder;
        this.submitted = false;
    }
    ngOnInit() {
        this.ResetForm = this.formBuilder.group({
            email: ['', [Validators.required, Validators.email]],
        });
    }
    navigate() {
        this.router.navigate(['login']);
        console.log("SUCCESS");
    }
    navigatemail() {
        this.router.navigate(['mail']);
        console.log("SUCCESS");
    }
    get f() { return this.ResetForm.controls; }
    // convenience getter for easy access to form fields
    onSubmit() {
        this.submitted = true;
        // stop here if form is invalid
        if (this.ResetForm.invalid) {
            return;
        }
        // display form values on success
        alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.ResetForm.value, null, 4));
    }
    onReset() {
        this.submitted = false;
        this.ResetForm.reset();
    }
};
__decorate([
    Output()
], ResetPasswordComponent.prototype, "mail", void 0);
ResetPasswordComponent = __decorate([
    Component({
        selector: 'app-reset-password',
        templateUrl: './reset-password.component.html',
        styleUrls: ['./reset-password.component.css']
    })
], ResetPasswordComponent);
export { ResetPasswordComponent };
//# sourceMappingURL=reset-password.component.js.map