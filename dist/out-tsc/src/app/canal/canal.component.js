import { __decorate } from "tslib";
import { Component } from '@angular/core';
let CanalComponent = class CanalComponent {
    constructor(fb) {
        this.fb = fb;
        this.isSubmitted = false;
        this.canaux = this.fb.group({
            canalPersonnalise: ['',],
            canalStandard: ['',]
        });
    }
    ngOnInit() {
    }
    setradio(e) {
        this.selectedLink = e;
    }
    isSelected(name) {
        if (!this.selectedLink) {
            return false;
        }
        return (this.selectedLink === name);
    }
    getValue() {
        alert(this.canaux.value);
    }
};
CanalComponent = __decorate([
    Component({
        selector: 'app-canal',
        templateUrl: './canal.component.html',
        styleUrls: ['./canal.component.css']
    })
], CanalComponent);
export { CanalComponent };
//# sourceMappingURL=canal.component.js.map