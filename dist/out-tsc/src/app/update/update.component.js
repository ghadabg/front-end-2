import { __decorate, __param } from "tslib";
import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Validators } from '@angular/forms';
let UpdateComponent = class UpdateComponent {
    constructor(notification, dialogRef, Service, fb) {
        this.notification = notification;
        this.dialogRef = dialogRef;
        this.Service = Service;
        this.fb = fb;
        this.parametreCompte = { Transactions: ['Alerte sur dépôt automatique', 'Alerte sur dépassement des dépenses', 'Alerte sur dépassement de seuil d’un crédit ', 'Alerte sur dépassement de seuil d’un débit', 'Alerte sur virement international reçu', ' Alerte d’échec des transferts'], Solde: ['Alerte sur solde d’un compte', 'Alerte sur dépassement du solde d’un seuil',] };
        this.parametreProduit = {
            Cartebancaire: ['Alerte sur dépassement du plafond d’une carte bancaire', 'Alerte sur blocage d’une carte bancaire', 'Alerte sur dated’éxpiration d’une carte bancaire ', 'Alerte sur paiement en ligne par une carte bancaire', 'Alerte sur échec de paiement en ligne par une carte bancaire', 'Alerte sur dépassement des dépenses par une carte bancaire'],
            Chequier: ['Alerte sur compensation d’un chéque', 'Alerte sur refus d’un chéque par la banque', 'Alerte sur échec de paiement par chéque']
        };
        if (!this.notification) {
            this.notification = {
                _id: undefined,
                CIN: undefined,
                ACCOUNT_No: undefined,
                Categorie: undefined,
                parametres: undefined,
                preference: {
                    choix: undefined,
                    montant: undefined,
                },
                canaux: {
                    canalPersonnalise: undefined,
                    canalStandard: undefined,
                }
            };
        }
    }
    ngOnInit() {
        this.updateForm = this.fb.group({
            oldparametres: ['', Validators.required],
            oldCategorie: ['', Validators.required],
            UpdatedCategorie: ['', Validators.required],
            UpdatedParametres: ['', Validators.required],
        });
    }
    onCancelClickHandler() {
        this.closeDialog({ message: this.DIALOG_CLOSE_MSGS.CANCEL });
    }
    onSubmit() {
    }
    onSaveClickHandler() {
        if (this.notification._id) {
            this.updateUser();
        }
        if (!this.notification._id) {
            this.addUser();
        }
    }
    onReset() {
        this.submitted = false;
        this.updateForm.reset();
        this.closeDialog({ message: this.DIALOG_CLOSE_MSGS.OK });
    }
    changeValue() {
        this.updateForm.value.oldCategorie = this.updateForm.value.UpdatedCategorie;
        this.updateForm.value.oldparametres = this.updateForm.value.UpdatedParametres;
    }
    setradio(e) { this.selectedLink = e; }
    isSelected(name) {
        if (!this.selectedLink) {
            return false;
        }
        return (this.selectedLink === name);
    }
    closeDialog(data) {
        this.dialogRef.close({ data: data });
    }
    addUser() {
        this.Service.addNotification(this.notification)
            .subscribe((res) => {
            this.closeDialog({ message: this.DIALOG_CLOSE_MSGS.OK });
        });
    }
    updateUser() {
        this.Service.updateNotification(this.notification._id, this.notification)
            .subscribe((res) => {
            this.closeDialog({ message: this.DIALOG_CLOSE_MSGS.OK });
        });
    }
};
UpdateComponent = __decorate([
    Component({
        selector: 'app-update',
        templateUrl: './update.component.html',
        styleUrls: ['./update.component.css']
    }),
    __param(0, Inject(MAT_DIALOG_DATA))
], UpdateComponent);
export { UpdateComponent };
//# sourceMappingURL=update.component.js.map