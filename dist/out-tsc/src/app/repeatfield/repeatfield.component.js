import { __decorate } from "tslib";
import { Component } from '@angular/core';
import { preference } from 'front-end-2/src/app/preference.model';
let RepeatfieldComponent = class RepeatfieldComponent {
    constructor(fb) {
        this.fb = fb;
        this.pref = new preference();
        this.Dataarray = [];
        this.reactiveForm();
    }
    ngOnInit() {
    }
    reactiveForm() {
        this.preference = this.fb.group({
            choix: [],
            montant: [],
        });
    }
    submitForm() {
        console.log(this.preference.value);
    }
    addForm() {
        this.pref = new preference();
        this.Dataarray.push(this.pref);
    }
    remove(index) {
        this.Dataarray.splice(index);
    }
};
RepeatfieldComponent = __decorate([
    Component({
        selector: 'app-repeatfield',
        templateUrl: './repeatfield.component.html',
        styleUrls: ['./repeatfield.component.css']
    })
], RepeatfieldComponent);
export { RepeatfieldComponent };
//# sourceMappingURL=repeatfield.component.js.map