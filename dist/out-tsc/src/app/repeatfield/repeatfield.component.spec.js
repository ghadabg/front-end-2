import { async, TestBed } from '@angular/core/testing';
import { RepeatfieldComponent } from './repeatfield.component';
describe('RepeatfieldComponent', () => {
    let component;
    let fixture;
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [RepeatfieldComponent]
        })
            .compileComponents();
    }));
    beforeEach(() => {
        fixture = TestBed.createComponent(RepeatfieldComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=repeatfield.component.spec.js.map