import { __decorate } from "tslib";
import { Component, ViewChild } from '@angular/core';
import { MatTableDataSource, MatTable } from '@angular/material/table';
import { UpdateComponent } from '../update/update.component';
import { MatSort, } from '@angular/material/sort';
import { MatPaginator, } from '@angular/material/paginator';
const ELEMENT_DATA = [];
let TableComponent = class TableComponent {
    constructor(dialog, changeDetectorRefs, apiService, serviceClient) {
        this.dialog = dialog;
        this.changeDetectorRefs = changeDetectorRefs;
        this.apiService = apiService;
        this.serviceClient = serviceClient;
        this.columnsToDisplay = ['CIN', 'catégorie', 'parametres', 'canaux', 'edit', 'delete'];
        this.dataSource = new MatTableDataSource();
        this.Types = {};
        this.getNotif();
    }
    ;
    ngOnInit() {
        this.getNotif();
    }
    onEditButtonClickHandler(notification) {
        this.openModalDialog(notification);
    }
    onChange(filterValues) {
        this.dataSource.filter = JSON.stringify(filterValues);
    }
    applyFilter(event) {
        const filterValue = event.target.value;
        this.dataSource.filter = filterValue.trim().toLowerCase();
    }
    openModalDialog(notification) {
        let dialog = this.dialog.open(UpdateComponent, { data: notification });
        dialog.afterClosed().subscribe(results => {
            if (results.data.message === 'OK') {
                this.getNotif();
            }
        });
    }
    getNotif() {
        this.apiService.getNotifications()
            .subscribe(data => {
            this.Notifications = data;
            this.dataSource.data = this.Notifications;
        });
        return this.Notifications;
    }
    deleteRow(not) {
        this.apiService.deleteNotificiation(not._id)
            .subscribe(data => {
            console.log('Deleted Value : ', data);
            this.dataSource.data.splice(this.dataSource.data.indexOf(not), 1);
            this.table.renderRows();
        });
    }
    toggleTheme() {
        this.mode = !this.mode;
    }
};
__decorate([
    ViewChild(MatPaginator)
], TableComponent.prototype, "paginator", void 0);
__decorate([
    ViewChild(MatSort)
], TableComponent.prototype, "sort", void 0);
__decorate([
    ViewChild(MatTable)
], TableComponent.prototype, "table", void 0);
TableComponent = __decorate([
    Component({
        selector: 'app-table',
        templateUrl: './table.component.html',
        styleUrls: ['./table.component.css']
    })
], TableComponent);
export { TableComponent };
//# sourceMappingURL=table.component.js.map