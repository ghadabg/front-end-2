import { __decorate } from "tslib";
import { Component } from '@angular/core';
import { preference } from 'front-end-2/src/app/preference.model';
let AddClientComponent = class AddClientComponent {
    constructor() {
        this.preference = new preference();
        this.Dataarray = [];
    }
    ngOnInit() {
    }
    addForm() {
        this.preference = new preference();
        this.Dataarray.push(this.preference);
    }
};
AddClientComponent = __decorate([
    Component({
        selector: 'app-add-client',
        templateUrl: './add-client.component.html',
        styleUrls: ['./add-client.component.css']
    })
], AddClientComponent);
export { AddClientComponent };
//# sourceMappingURL=add-client.component.js.map