import { async, TestBed } from '@angular/core/testing';
import { PersonnalisationComponent } from './personnalisation.component';
describe('PersonnalisationComponent', () => {
    let component;
    let fixture;
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [PersonnalisationComponent]
        })
            .compileComponents();
    }));
    beforeEach(() => {
        fixture = TestBed.createComponent(PersonnalisationComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=personnalisation.component.spec.js.map