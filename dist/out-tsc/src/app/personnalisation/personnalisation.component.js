import { __decorate } from "tslib";
import { Component } from '@angular/core';
let PersonnalisationComponent = class PersonnalisationComponent {
    constructor(notification, Service) {
        this.notification = notification;
        this.Service = Service;
    }
    ngOnInit() {
    }
    closeDialog(data) {
    }
};
PersonnalisationComponent = __decorate([
    Component({
        selector: 'app-personnalisation',
        templateUrl: './personnalisation.component.html',
        styleUrls: ['./personnalisation.component.css']
    })
], PersonnalisationComponent);
export { PersonnalisationComponent };
//# sourceMappingURL=personnalisation.component.js.map