import { __decorate } from "tslib";
import { Component, ViewChild } from '@angular/core';
import { Validators } from '@angular/forms';
import { preference } from 'front-end-2/src/app/preference.model';
import { CanalComponent } from '../canal/canal.component';
import { RepeatfieldComponent } from '../repeatfield/repeatfield.component';
let FormsComponent = class FormsComponent {
    constructor(fb, apiservice, accountserrvice, clientService) {
        this.fb = fb;
        this.apiservice = apiservice;
        this.accountserrvice = accountserrvice;
        this.clientService = clientService;
        this.submitted = false;
        this.notfound = false;
        this.selectItem = [];
        this.columnsToDisplay = ['Client CIN', 'Numero de compte', 'Nom et Prénom', 'Catégorie du notification', 'Nom du notification', 'Type du notification'];
        this.preference = new preference();
        this.Dataarray = [];
        this.parametreCompte = { Transactions: ['Alerte sur dépôt automatique', 'Alerte sur dépassement des dépenses', 'Alerte sur dépassement de seuil d’un crédit ', 'Alerte sur dépassement de seuil d’un débit', 'Alerte sur virement international reçu', ' Alerte d’échec des transferts'], Solde: ['Alerte sur solde d’un compte', 'Alerte sur dépassement du solde d’un seuil',] };
        this.parametreProduit = {
            Cartebancaire: ['Alerte sur dépassement du plafond d’une carte bancaire', 'Alerte sur blocage d’une carte bancaire', 'Alerte sur dated’éxpiration d’une carte bancaire ', 'Alerte sur paiement en ligne par une carte bancaire', 'Alerte sur échec de paiement en ligne par une carte bancaire', 'Alerte sur dépassement des dépenses par une carte bancaire'],
            Chequier: ['Alerte sur compensation d’un chéque', 'Alerte sur refus d’un chéque par la banque', 'Alerte sur échec de paiement par chéque']
        };
    }
    addForm() {
        this.preference = new preference();
        this.Dataarray.push(this.preference);
    }
    onSubmit() {
        this.ParametreForm.addControl('canaux', this.canal.canaux);
        this.canal.canaux.setParent(this.ParametreForm);
        this.ParametreForm.addControl('preference', this.Config.preference);
        this.Config.preference.setParent(this.ParametreForm);
        this.apiservice.addNotification(this.ParametreForm.value).subscribe((response) => console.log(response), (error) => console.log(error));
        console.log(this.ParametreForm.value);
    }
    getNotif() {
        this.apiservice.getNotifications()
            .subscribe(data => {
            this.notifications = data;
            this.dataSource = this.notifications;
            console.log(this.dataSource);
        });
    }
    getAccount(cin) {
        this.clientService.getCLientByCIN(cin).subscribe(data => {
            this.client = data;
            console.log(this.client);
            if (this.client.length == 0) {
                this.notfound = true;
            }
            else {
                console.log(this.notfound);
            }
            console.log(this.client[0].ID);
            this.accountserrvice.getAccountsbyClient(this.client[0].ID).subscribe(data => {
                this.account = data;
                console.log(this.account);
            });
        });
    }
    setradio(e) { this.selectedLink = e; }
    isSelected(name) {
        if (!this.selectedLink) {
            return false;
        }
        return (this.selectedLink === name);
    }
    ngOnInit() {
        this.ParametreForm = this.fb.group({
            CIN: ['', Validators.required],
            ACCOUNT_No: ['', Validators.required],
            Categorie: ['', Validators.required],
            parametres: ['', Validators.required],
        });
    }
    SelectedArray() {
        this.selectItem.push(this.selected);
    }
    onRemove() {
        this.selectItem.pop();
    }
    getselectedvalue() {
        return this.selected;
    }
};
__decorate([
    ViewChild(CanalComponent)
], FormsComponent.prototype, "canal", void 0);
__decorate([
    ViewChild(RepeatfieldComponent)
], FormsComponent.prototype, "Config", void 0);
FormsComponent = __decorate([
    Component({
        selector: 'app-forms',
        templateUrl: './forms.component.html',
        styleUrls: ['./forms.component.css']
    })
], FormsComponent);
export { FormsComponent };
//# sourceMappingURL=forms.component.js.map