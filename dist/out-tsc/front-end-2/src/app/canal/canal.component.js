import { __decorate } from "tslib";
import { Component } from '@angular/core';
import { canal } from '../canal.model';
let CanalComponent = class CanalComponent {
    constructor() {
        this.canal = new canal();
        this.mailarray = [];
        this.change = false;
    }
    ngOnInit() { }
    addcanal() {
        this.canal = new canal();
        this.mailarray.push(this.canal);
    }
    back(index) {
        this.mailarray.splice(index);
    }
};
CanalComponent = __decorate([
    Component({
        selector: 'app-canal',
        templateUrl: './canal.component.html',
        styleUrls: ['./canal.component.css']
    })
], CanalComponent);
export { CanalComponent };
//# sourceMappingURL=canal.component.js.map