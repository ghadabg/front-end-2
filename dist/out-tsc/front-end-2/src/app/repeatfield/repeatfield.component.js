import { __decorate } from "tslib";
import { Component } from '@angular/core';
import { preference } from '../preference.model';
let RepeatfieldComponent = class RepeatfieldComponent {
    constructor() {
        this.preference = new preference();
        this.Dataarray = [];
    }
    ngOnInit() {
    }
    addForm() {
        this.preference = new preference();
        this.Dataarray.push(this.preference);
    }
    remove(index) {
        this.Dataarray.splice(index);
    }
};
RepeatfieldComponent = __decorate([
    Component({
        selector: 'app-repeatfield',
        templateUrl: './repeatfield.component.html',
        styleUrls: ['./repeatfield.component.css']
    })
], RepeatfieldComponent);
export { RepeatfieldComponent };
//# sourceMappingURL=repeatfield.component.js.map