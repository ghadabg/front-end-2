import { __decorate } from "tslib";
import { Component, Output } from '@angular/core';
let ResetPasswordComponent = class ResetPasswordComponent {
    constructor(router) {
        this.router = router;
    }
    ngOnInit() {
        throw new Error("Method not implemented.");
    }
    navigate() {
        this.router.navigate(['login']);
        console.log("SUCCESS");
    }
    navigatemail() {
        this.router.navigate(['mail']);
        console.log("SUCCESS");
    }
};
__decorate([
    Output()
], ResetPasswordComponent.prototype, "mail", void 0);
ResetPasswordComponent = __decorate([
    Component({
        selector: 'app-reset-password',
        templateUrl: './reset-password.component.html',
        styleUrls: ['./reset-password.component.css']
    })
], ResetPasswordComponent);
export { ResetPasswordComponent };
//# sourceMappingURL=reset-password.component.js.map