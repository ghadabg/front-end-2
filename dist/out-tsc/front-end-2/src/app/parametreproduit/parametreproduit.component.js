import { __decorate } from "tslib";
import { Component } from '@angular/core';
import { ParametreProduit } from './ParametreProduit.model';
let ParametreproduitComponent = class ParametreproduitComponent {
    constructor() {
        this.ProduitArray = [];
        this.parametreProduit = new ParametreProduit();
    }
    ngOnInit() {
    }
    addparametreProduit() {
        this.parametreProduit = new ParametreProduit();
        this.ProduitArray.push(this.parametreProduit);
    }
};
ParametreproduitComponent = __decorate([
    Component({
        selector: 'app-parametreproduit',
        templateUrl: './parametreproduit.component.html',
        styleUrls: ['./parametreproduit.component.css']
    })
], ParametreproduitComponent);
export { ParametreproduitComponent };
//# sourceMappingURL=parametreproduit.component.js.map