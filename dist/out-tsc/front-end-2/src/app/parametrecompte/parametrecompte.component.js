import { __decorate } from "tslib";
import { Component } from '@angular/core';
import { parametreCompte } from './parametreCompte.model';
import { preference } from '../preference.model';
let ParametrecompteComponent = class ParametrecompteComponent {
    constructor() {
        this.compteArray = [];
        this.parametrecompte = new parametreCompte();
        this.preference = new preference();
        this.DattaArray = [];
    }
    ngOnInit() {
    }
    addparametrecompte() {
        this.parametrecompte = new parametreCompte();
        this.compteArray.push(this.parametrecompte);
    }
};
ParametrecompteComponent = __decorate([
    Component({
        selector: 'app-parametrecompte',
        templateUrl: './parametrecompte.component.html',
        styleUrls: ['./parametrecompte.component.css']
    })
], ParametrecompteComponent);
export { ParametrecompteComponent };
//# sourceMappingURL=parametrecompte.component.js.map