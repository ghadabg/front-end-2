import { Component, OnInit, ViewChild, ChangeDetectorRef, Input } from '@angular/core';
import {MatTableDataSource, MatTable} from '@angular/material/table';
import { ApiNotifService } from 'src/shared/api-notif.service';
import{Notification} from 'src/app/Models/Notification.model'
import { MatDialog } from '@angular/material/dialog';
import { ApiClientService } from 'src/shared/apiclient.service';
import { Client } from '../Models/Client';
import { FormGroup } from 'front-end-2/node_modules/@angular/forms/forms';
import { Observable } from 'rxjs';
import { UpdateComponent } from '../update/update.component';
import { MatSort, } from '@angular/material/sort';
import { MatPaginator, } from '@angular/material/paginator';
import { Data } from 'front-end-2/node_modules/@angular/router/router';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { MultiDataSet, Label, Color } from 'ng2-charts';
import * as Chart from 'chart.js';
import Swal from 'sweetalert2'

const ELEMENT_DATA: Notification[]=[]

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {
  columnsToDisplay: string[] = ['CIN', 'compte','catégorie', 'parametres', 'canal','edit', 'delete'];
  CountPerso: any;
  CanalSt: any;
  CountSt: any;
  l:any;
 dataSource = new MatTableDataSource<Data>();
  paginator: MatPaginator;
 
 @ViewChild(MatPaginator) set matPaginator(mp: MatPaginator) {
  this.paginator = mp;
  this.setDataSourceAttributes();
  
} @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatTable) table: MatTable<any>;
  Notifications:Notification[]
  Notification:   Notification;
  editField: any;
  Types: any={};

  userTable:FormGroup
  usersName:Client[];
  LineChart: Chart;
  notif: any;
  constructor(   private dialog: MatDialog, private changeDetectorRefs: ChangeDetectorRef,
    public apiService: ApiNotifService,public serviceClient:ApiClientService){
  this.getNotif()
 
}
  ngOnInit() 
  {  
     this.getNotif()
   
   
   }
   setDataSourceAttributes() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }
   onEditButtonClickHandler(notification: Notification): void {
  this.openModalDialog(notification);
  }

  onChange(filterValues: Object) {
     this.dataSource.filter = JSON.stringify(filterValues); }


     applyFilter(event: Event) {
      const filterValue = (event.target as HTMLInputElement).value;
      this.dataSource.filter = filterValue.trim().toLowerCase();
    }
  openModalDialog(notification: Notification): void {
   let dialog = this.dialog.open(UpdateComponent, { data: notification });

    dialog.afterClosed().subscribe(results => {
      if (results.data.message === 'OK') {
      this.getNotif();
     }
});
}
deleterequest(notif:Notification){
  Swal.fire({
    title: 'Êtes-vous sûr?',
    text: "Vous ne pourrez pas revenir sur cela!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#008000',
    cancelButtonColor: '#FF0000	',
    confirmButtonText: 'Oui, je suis sûr!'
  }).then((result) => {
    if (result.value) {
      this.deleteRow(notif)
      Swal.fire(
        'Supprimée !',
        'Votre notification a été supprimée .',
        'success'
      )
    }
  })
}

ngAfterViewInit() {
  this.dataSource.paginator = this.paginator
}
  getNotif() {
    this.apiService.getNotifications()
    .subscribe(data => { 
      this.Notifications=data
      this.dataSource.data=this.Notifications
    }
    )
    return this.Notifications
}

c

  deleteRow( not:Notification) {

    this.apiService.deleteNotificiation(not._id)
      .subscribe(data => { 
        console.log('Deleted Value : ',data)
        this.dataSource.data.splice(this.dataSource.data.indexOf(not), 1);
        this.table.renderRows();

      });
}

}