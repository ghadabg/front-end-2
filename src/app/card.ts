export class Card {
     CARD_NUMBER : String;
     CLIENT_CODE : String;
     ACCOUNT : String;
     CARD_NAME : String;
     FIRST_NAME : String;
     LAST_NAME : String;
     BIRTH_DATE : String;
     ADDRESS : String;
     EMAIL : String;
     PHONE : String;}
