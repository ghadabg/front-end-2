export class Account {
    id:String;
    ACCOUNT:String;
    Client:String;
    Currency:String;
    "First Name":String;
    "Last Name":String;
    Product:String;
    "Opening Date":Date;
    Amount: String;
}

