import { Component, OnInit } from '@angular/core';
import * as Chart from 'chart.js';
import { ApiNotifService } from 'src/shared/api-notif.service';
import { Label, Color, MultiDataSet } from 'ng2-charts';
import { ChartType } from 'chart.js';

@Component({
  selector: 'app-charts',
  templateUrl: './charts.component.html',
  styleUrls: ['./charts.component.css']
})
export class ChartsComponent implements OnInit {
  CountSms:any;
  
  CountMail: any;

  PieChart: Chart;
  responseData: any;
  count: any;
  doughnutChartData: MultiDataSet = [
    [this.count ]
  ];

  constructor(public apiService:ApiNotifService) {}
notif:any
  ngOnInit(): void {
 this.getData();
  }
  // ngAfterViewInit(){
  //   this.CountCanalMail()
  //   this.CountCanalSms()
  //   this.CountNotifications()
  //   this.chart()
  //   this.chart1()

  // }


  async getPromise() {
    const promiseGet= await this.apiService.GetCountDOC().toPromise()
     return promiseGet;
    
   } 
   doughnutChartLabels: Label[] =["Nombre des notifications envoyées"];
  
  public doughnutChartColors: Color[] = [
   
    {backgroundColor:['rgba(17, 190, 176, 0.842)']},
    
  ];
  doughnutChartType: ChartType = 'doughnut';
   async getPromise1() {
    const promiseGet= await this.apiService.CountCanalmail().toPromise()
     return promiseGet;
    
   } 
   async getPromise2() {
    const promiseGet= await this.apiService.CountCanalsms().toPromise()
     return promiseGet;
    
   } 
   async getData(){
    await this.getPromise().then((res)=>{
      console.log('stat',res)
      this.count = res
    }); console.log('val',this.count);

    await this.getPromise1().then((res)=>{
      console.log('stat',res)
      this.CountMail = res
    }); console.log('val1',this.CountMail);
    await this.getPromise2().then((res)=>{
      console.log('CountSms',res)
      this.CountSms = res
    }); console.log('CountSms',this.CountSms);


    // this.PieChart = new Chart('pieChart', {
    //   type: 'pie',
    // data: {
    //  labels:  ['Notifications '],
    //  datasets: [{
    //      label: 'Nombre des notifications envoyées',
    //      data: [this.count],
    //      fill:true,
    //      lineTension:0.2,
    //     //  borderColor:"green",
    //      borderWidth: 1
    //  }]
    // }, 
    // options: {
    //  title:{
    //      text:"Line Chart",
    //      display:true
    //  },
    //  scales: {
    //      yAxes: [{
    //          ticks: {
    //              beginAtZero:true
    //          }
    //      }]
    //  }
    // }
    
    // });
    
    this.doughnutChartData =[
      [ this.count]
    ]

    new Chart("bar-chart", {
      type: 'horizontalBar',
      data: {
        labels: ['Le canal sms','Le canal mail'],
        datasets: [
          {
            label: "Canaux ",
              
            backgroundColor: [
              'rgba(255, 99, 132, 0.2)',
              'rgba(54, 162, 235, 0.2)',
            ],
            borderColor: [
              'rgba(255, 99, 132, 1)',
              'rgba(54, 162, 235, 1)',
            ],
            borderWidth: 1
        ,
            data: [1,2]
          }
        ]
      },
      options: {
        legend: { display: false },
        title: {
          display: true,
          text: 'Les canaux des notifications'
        },
        scales: {
          xAxes: [{
            ticks: {
              beginAtZero: true,            
          }
        }]
  
        ,
          yAxes: [{
              stacked: true
          }]
      }
  }
  });
  }
  // chart1(){
  //   this.LineChart = new Chart('lineChart', {
  //     type: 'line',
  //   data: {
  //    labels:  ['Notifications '],
  //    datasets: [{
  //        label: 'Nombre des notifications envoyées',
  //        data: [this.count],
  //        fill:false,
  //        lineTension:0.2,
  //        borderColor:"green",
  //        borderWidth: 1
  //    }]
  //   }, 
  //   options: {
  //    title:{
  //        text:"Line Chart",
  //        display:true
  //    },
  //    scales: {
  //        yAxes: [{
  //            ticks: {
  //                beginAtZero:true
  //            }
  //        }]
  //    }
  //   }
    
  //   });
    
    
    
  //   }
    // chart(){
    //   new Chart("bar-chart", {
    //     type: 'horizontalBar',
    //     data: {
    //       labels: ['Le canal sms','Le canal mail'],
    //       datasets: [
    //         {
    //           label: "Canaux ",
                
    //           backgroundColor: [
    //             'rgba(255, 99, 132, 0.2)',
    //             'rgba(54, 162, 235, 0.2)',
    //           ],
    //           borderColor: [
    //             'rgba(255, 99, 132, 1)',
    //             'rgba(54, 162, 235, 1)',
    //           ],
    //           borderWidth: 1
    //       ,
    //           data: [this.CountSms,this.CountMail]
    //         }
    //       ]
    //     },
    //     options: {
    //       legend: { display: false },
    //       title: {
    //         display: true,
    //         text: 'Les canaux des notifications'
    //       },
    //       scales: {
    //         xAxes: [{
    //           ticks: {
    //             beginAtZero: true,            
    //         }
    //       }]
    
    //       ,
    //         yAxes: [{
    //             stacked: true
    //         }]
    //     }
    // }
    // });
    // }
    // async CountNotifications(){
    //   this.count= await  this.apiService.GetCountDOC()
    //   console.log('Notifications :',this.count)

    // }
    // async CountCanalSms(){
    //   this.CountSms= await  this.apiService.CountCanalmail()
    //   console.log("SMS : ",this.CountSms)

     
    // }
    //  async CountCanalMail(){
    //  this.CountMail=await this.apiService.CountCanalmail()
    //  console.log('MAIL :',this.CountMail)
    // }
    

}
