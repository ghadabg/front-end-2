import { Component, OnInit, Inject, Output, EventEmitter, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators} from '@angular/forms';
import { MatDialog, MatDialogConfig,MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-canal',
  templateUrl: './canal.component.html',
  styleUrls: ['./canal.component.css']
})
export class CanalComponent implements OnInit {
  private selectedLink: String; 
  isSubmitted = false;
  canaux:FormGroup
   constructor(public fb: FormBuilder,public dialog: MatDialog )
     {
      this.canaux = this.fb.group({
        canal: ['', ],
      })}
     
ngOnInit(){

  
}
setradio(e: string): void   
{  
    this.selectedLink = e;  

}  
isSelected(name: string): boolean   
{  
    if (!this.selectedLink) { 
        return false;  
}  

    return (this.selectedLink === name);
} 
getValue()
{alert(this.canaux.value)

}
}
