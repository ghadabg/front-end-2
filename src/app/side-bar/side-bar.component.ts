import { Component, OnInit } from '@angular/core';
import * as Chart from 'chart.js';
import { ApiNotifService } from 'src/shared/api-notif.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.css']
})
export class SideBarComponent implements OnInit {

  CountPerso: any;
  LineChart: Chart;
  CountSt: number | number[] | Chart.ChartPoint;

  constructor(public apiService:ApiNotifService,private router:Router) { }
  ngOnInit(): void {
  }
  


}
