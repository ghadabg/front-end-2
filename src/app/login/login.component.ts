import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserService } from '../user.service';
import { User } from '../Models/User';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
LoginForm:FormGroup;
submitted = false;
isCorrect=false;
  user:User[]
  identification: boolean;
  errorMessage: string;
  vide=false
  constructor(private router:Router,private formBuilder: FormBuilder,public userService:UserService) {
   }
  ngOnInit(): void { this.LoginForm = this.formBuilder.group({
    email: ['',[Validators.required, Validators.email]],
    password: ['',Validators.required]})
}
onSubmit() {
  this.submitted = true;
  // stop here if form is invalid
  if (this.LoginForm.invalid) {
      this.isCorrect=true;  
  }
  // display form values on success
}
get f() { return this.LoginForm.controls; }

Vide(){
  if ( this.LoginForm.value.email ==='' &&this.LoginForm.value.password ===''){
    this.vide=true;
  }
}  

navigateToAdmin() {
  if(this.LoginForm.valid)
  {  this.userService.getUsers().subscribe(data=>
  {this.user=data
    if ( this.LoginForm.value.email === this.user[0].email && this.LoginForm.value.password=== this.user[0].password)
     {
      this.identification=true}
      else {
        this.identification=false
        this.errorMessage ='Adresse e-mail ou mot de passe invalide.'
    }
    console.log(this.identification)
    if (this.identification==true) {
      this.router.navigate(['admin']);
      
    }
  })}}
}

  
