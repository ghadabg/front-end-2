import { Component, OnInit, Input, ViewChild ,AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, FormArray } from '@angular/forms';
import { preference } from 'front-end-2/src/app/preference.model';
import { CanalComponent } from '../canal/canal.component';
import { RepeatfieldComponent } from '../repeatfield/repeatfield.component';
import { ApiNotifService } from 'src/shared/api-notif.service';
import { AccountService } from 'src/shared/account.service';
import { ApiClientService } from 'src/shared/apiclient.service';
import { Client } from '../Models/Client';
import { Account } from '../Models/account';
import { MatDialog } from '@angular/material/dialog';
import Swal from 'sweetalert2';
import { Card } from '../card';
import { NgControl } from '@angular/forms';

@Component({
  selector: 'app-forms',
  templateUrl: './forms.component.html',
  styleUrls: ['./forms.component.css']
})
export class FormsComponent implements OnInit {
 // @ViewChild(CanalComponent) canal : CanalComponent;

  @ViewChild(RepeatfieldComponent) Config :RepeatfieldComponent;
  submitted =false;
  notfound=false
  value: String
  selected :String;
  selectItem: String[] = [];
  columnsToDisplay: string[] = ['Client CIN' ,'Numero de compte','Nom et Prénom',  'Catégorie du notification', 'Nom du notification','Type du notification'];
  preference=new preference();
  Dataarray=[];  
  ParametreForm:FormGroup
  parametreCompte={Transactions :["VIREMENT INTRA", "CERTIFICATION CHEQUE", "Prelevement recu"]}
  parametreProduit={
    Cartebancaire:['Achat Porteur BZ, Acquéreur Conf', 'Retrait Porteur BZ, Acquéreur Conf',"ACHAT TPE A L'ETRANGER PORTEUR BZf"],
 
  }
  Categorie:FormControl
 
  selectedLink: String;   
  parametre: FormGroup;
  canalInvalide=false;
  errorMessage: string;
  AccounterrorMessage: string;
;
  dataSource: Notification[];
  notifications:any;
  client:Client 
  account :Account[]
  ac : Account;
  selectedData: any
  isCorrect: boolean;
  cards:Card[]
  card:Card;
  accounts: any;
  InvalideAccount=false;
    constructor(public fb: FormBuilder , private apiservice: ApiNotifService,private accountserrvice: AccountService, private clientService : ApiClientService) { 
  }
 
 
  addForm(){
    this.preference=new preference();
    this.Dataarray.push(this.preference);
  }

   getCategorieValue(){
     return (this.ParametreForm.value.Categorie) 
   }
  
  onSubmit() {
    this.ParametreForm.addControl('preference', this.Config.preference);
    this.Config.preference.setParent(this.ParametreForm);
if(this.ParametreForm.valid){
   this.apiservice.addNotification(this.ParametreForm.value).subscribe(
      (response) => console.log(response),
      (error) => console.log(error)
      
    )
    let timerInterval
    Swal.fire({
      title: 'Nous sauvegardons vos données',
      html: '',
      timer: 1500,
      timerProgressBar: true,
      onBeforeOpen: () => {
        Swal.showLoading()
        timerInterval = setInterval(() => {
          const content = Swal.getContent()
          if (content) {
            const b = content.querySelector('b')
            if (b) {
              b.textContent = Swal.getTimerLeft().toString();
            }
          }
        }, 100)
      },
      onClose: () => {
        clearInterval(timerInterval)
      }
    }).then((result) => {
      /* Read more about handling dismissals below */
      if (result.dismiss === Swal.DismissReason.timer) {
      }
    })}
if(this.getCanalValue()==='')  {
  this.canalInvalide=true 
  this.errorMessage ='Type de la notification est requis'
} 
 else if(this.ParametreForm.valid ){this.submitted=true
   
 }
 console.log('sub ',this.submitted)
 } 
 
  
  getCanalValue(){
    return this.ParametreForm.value.canal

  }

    getNotif() : void {
      this.apiservice.getNotifications()
      .subscribe(data => {
          this.notifications = data;
          this.dataSource = this.notifications;
          console.log(this.dataSource)
      });
    }
  
getAccount(cin : String) {
  this.clientService.getCLientByCIN(cin).subscribe(data => {
    this.client=data
   console.log(this.client)
   if (this.client==null){
    Swal.fire({
      icon: 'error',
      title: 'Oops...',
      text: 'CIN invalide !',
    })
    this.setradio('')

   
  }  else  {console.log(this.notfound)
  
  }
    console.log(this.client.ID)
    this.accountserrvice.getAccountsbyClient(this.client.ID).subscribe(data => {this.account=data

    
  })
  
this.setradio('yes')

})}

resetSelected(){
  this.ParametreForm.value.ACCOUNT_NO=''

}
  setradio(e: string): void   
{   this.selectedLink = e; } 
  isSelected(name: string): boolean   
{  
if (!this.selectedLink)
 {  
 return false;  
} 
 return (this.selectedLink === name);   
}   
  ngOnInit(): void {    this.ParametreForm = this.fb.group({
    CIN:['',Validators.required],
    ACCOUNT_NO:['',Validators.required],
    Categorie:['',Validators.required],
    parametres: ['',Validators.required],
    canal: ['',Validators.required],
  })
   }
SelectedArray(){
  this.selectItem.push(this.selected);
}
onRemove(){
  this.selectItem.pop()}
getselectedvalue(){
return this.selected
   }
   
 Onresert(index){
  this.selectItem.splice(index, 1);
  
 } 

  
get f() { return this.ParametreForm.controls; }

}
    
