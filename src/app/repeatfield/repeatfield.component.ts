import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, FormArray } from '@angular/forms';
import { preference } from 'front-end-2/src/app/preference.model';

@Component({
  selector: 'app-repeatfield',
  templateUrl: './repeatfield.component.html',
  styleUrls: ['./repeatfield.component.css']
})
export class RepeatfieldComponent implements OnInit {
  FieldForm:FormGroup;
  pref=new preference();
  Dataarray=[];
  selected :String
  preference: FormGroup;
  value :String;
  constructor(private fb: FormBuilder) {
    this.reactiveForm()}
  ngOnInit()  { 
  }
  reactiveForm() {
    this.preference = this.fb.group({ 
          choix: [''],
          montant :[""],
       
     })
    }
  submitForm() {
    console.log(this.preference.value)
  }
  addForm(){
    this.pref=new preference();
    this.Dataarray.push(this.pref);
  }

  remove(index){this.Dataarray.splice(index);
  }

}
 
