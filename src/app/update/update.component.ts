import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ApiNotifService } from 'src/shared/api-notif.service';
import{Notification} from 'src/app/Models/Notification.model'
import { FormControl, FormBuilder, RequiredValidator, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2/dist/sweetalert2.js';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.css']
})
export class UpdateComponent implements OnInit {
  [x: string]: any;
  canaux=["Sms","Email"]
  parametreCompte={Transactions :["VIREMENT INTRA", "CERTIFICATION CHEQUE", "Prelevement recu"]}
  parametreProduit={
    Cartebancaire:['Achat Porteur BZ, Acquéreur Conf', 'Retrait Porteur BZ, Acquéreur Conf',"ACHAT TPE A L'ETRANGER PORTEUR BZf"],
 
  }
  updateForm:FormGroup
  oldUpdateForm :FormGroup
  constructor(   @Inject(MAT_DIALOG_DATA) public notification : Notification
,  public dialogRef: MatDialogRef<any>, private Service :ApiNotifService ,private fb:FormBuilder) {if (!this.notification) {
  this.notification= {
    _id:undefined,
     CIN:undefined,
     ACCOUNT_NO:undefined,
    Categorie:undefined,
    parametres:undefined,
    preference:{ 
        choix : undefined,
    montant :undefined,},
      canal :undefined,
      
  };
}  }
  ngOnInit() {
     this.updateForm=this.fb.group({
      oldparametres :new FormControl({ value: '', disabled: this.disabled }),
      oldCategorie : new FormControl({ value: '', disabled: this.disabled }),
      UpdatedCategorie: ['',],
      UpdatedParametres : ['',],
      oldCanal : ['',],
      UpdatedCanal : ['',],


      
      
    })
    this.updateForm.controls["oldparametres"].disable(),
    this.updateForm.controls["oldCategorie"].disable(),
    this.updateForm.controls["oldCanal"].disable()

  }

onCancelClickHandler(): void {
  this.closeDialog({ message: this.DIALOG_CLOSE_MSGS.CANCEL ,
   });
}
changeValueCanal(){

  this.updateForm.patchValue({
    
    oldCanal : [this.updateForm.value.UpdatedCanal]
    
    
  })}
onSubmit(){              
  
}
onSaveClickHandler(): void {
  if (this.notification._id) {
    this.updateUser();
  }
  if (!this.notification._id) {
    this.addUser();
  }
}

onReset() {
  this.closeDialog({ message: this.DIALOG_CLOSE_MSGS.OK });

}

changeValueCategorie(){
  this.updateForm.patchValue({
    
    oldCategorie : this.updateForm.value.UpdatedCategorie
    
    
  })}
  changeValueParametre(){
    this.updateForm.patchValue({    
      oldparametres : this.updateForm.value.UpdatedParametres
})}
setradio(e: string): void   
{   this.selectedLink = e; } 
  isSelected(name: string): boolean   
{  
if (!this.selectedLink)
 {  
 return false;  
} 
 return (this.selectedLink === name);   
}   
closeDialog(data: any): void {
  this.dialogRef.close({ data: data });
}

addUser(): void {
  this.Service.addNotification(this.notification)
    .subscribe((res: any) => {
      this.closeDialog({ message: this.DIALOG_CLOSE_MSGS.OK });
    });
}
getNotif() {
  this.apiService.getNotifications()
  .subscribe(data => { 
    this.Notifications=data
  })
  return this.Notifications
}
updateUser(): void {
  console.log(this.notification)
  console.log(this.updateForm.value)
  this.Service.updateNotification(this.notification._id,  this.notification)
    .subscribe((res: any) => { this.dialogRef.close();
     

})
Swal.fire({
  position: 'center',
  icon: 'success',
  title: 'votre notification a été enregistrée',
  showConfirmButton: false,
  timer: 1500
}).then((result) => {
  if (result.value) {
  
    
  }
})
this.getNotif();
}





}
