import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import {Account} from '../app/Models/account';
import {Card} from "../app/card"
const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

const REST_API_SERVER = 'http://localhost:3000/account/';
const CARD_URL ='http://localhost:3000/card/';

@Injectable({
  providedIn: 'root'
})

export class AccountService {

  constructor(private http: HttpClient) { 
  }
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    };
  }
  getAccounts(): Observable<Account[]> {
    return this.http.get<Account[]>(REST_API_SERVER, httpOptions)
    .pipe(catchError(this.handleError('getAccounts', []))
    );
  }
  getAccountsbyNo(no : String): Observable<Account[]> {
    const url = REST_API_SERVER+no;
    return this.http.get<Account[]>(url, httpOptions)
    .pipe(catchError(this.handleError('GetAccountByNo',[]))
    );
  }
  getAccountsbyClient(cl : String): Observable<Account[]> {
    const url = REST_API_SERVER+'client/'+cl;
    return this.http.get<Account[]>(url, httpOptions)
    .pipe(catchError(this.handleError('GetAccountByClient',[]))
    );
  }
  geCardbyAccount(Account : String): Observable<Card[]> {
    const url = CARD_URL+Account;
    return this.http.get<Card[]>(url, httpOptions)
    .pipe(catchError(this.handleError('GetAccountByClient',[]))
    );
  }
}
