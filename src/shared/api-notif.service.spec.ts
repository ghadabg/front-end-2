import { TestBed } from '@angular/core/testing';

import { ApiNotifService } from './api-notif.service';

describe('ApiNotifService', () => {
  let service: ApiNotifService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ApiNotifService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
