import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient ,HttpParams} from '@angular/common/http';
import { Observable, of } from 'rxjs';
import {catchError, map, tap} from 'rxjs/operators';
import { Client } from 'src/app/Models/Client';
const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

const REST_API_SERVER = 'http://localhost:3000/apiclient/';

@Injectable({
  providedIn: 'root'
})
export class ApiClientService {
  result: any;

  constructor(private http: HttpClient) { }
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    };
  }
  //get ALL Clients
  getClients(): Observable<Client[]> {
    return this.http.get<Client[]>(REST_API_SERVER, httpOptions)
    .pipe(catchError(this.handleError('getClients', []))
    );
  }
  //get Client by id
  getCLientById(id: String): Observable<Client> {
    const url = REST_API_SERVER+'id/'+id;
    return this.http.get<Client>(url).pipe(
      catchError(this.handleError<Client>(`getClientById=${id}`))
    );
  }
   //get Client by Cin
   getCLientByCIN(cin: String): Observable<Client> {
    const url = REST_API_SERVER+'CIN/'+cin;
    return this.http.get<Client>(url).pipe(
      catchError(this.handleError<Client>(`getClientByCIN=${cin}`))
    );
  }
     //get Client by CUSTOMER
     getCLientByCustomer(cl: string): Observable<Client> {
      const url =REST_API_SERVER+'client/'+cl;
      return this.http.get<Client>(url).pipe(
        catchError(this.handleError<Client>(`getClientByCIN=${cl}`))
      );
    }
     //get Client Name
      getCLientByName(cl: String):any  {
      const url = REST_API_SERVER+'Nom/'+cl;
      return this.http.get<Client>(url).pipe(
        catchError(this.handleError<Client>(`getClientByCIN=${cl}`))
      );
  }
      //get Client Mail
      getCLientByMail(cl: String): Observable<Client> {
        const url = REST_API_SERVER+'Mail/'+cl;
        return this.http.get<Client>(url).pipe(
          catchError(this.handleError<Client>(`getClientMail=${cl}`))
        );
      }
      addClient(client: Client): Observable<Client> {
        return this.http.post<Client>(REST_API_SERVER, client, httpOptions).pipe(
          catchError(this.handleError<Client>('addClient'))
        );
      }
      updateClient(id: String, client: Client): Observable<any> {
        const url =REST_API_SERVER+'/'+id;
        return this.http.put(url, client, httpOptions).pipe(
          catchError(this.handleError<any>('updateCLient'))
        );
      }
      deleteClient(id: String): Observable<Client> {
        const url = REST_API_SERVER+'/'+id;
        return this.http.delete<Client>(url, httpOptions)
          .pipe(
          catchError(this.handleError<Client>('deleteCLient'))
        );
      }
    
    

}
