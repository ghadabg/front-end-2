import { Injectable } from '@angular/core';
import { Observable, of, throwError, from } from 'rxjs';
import { HttpClient, HttpHeaders, HttpParams , HttpErrorResponse } from '@angular/common/http';
import{Notification} from 'src/app/Models/Notification.model'
import {catchError, map, tap} from 'rxjs/operators';
import { FormGroup } from 'front-end-2/node_modules/@angular/forms/forms';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};
const apiUrl = 'http://localhost:3000/apiNotif';
const   UrlCanal='http://localhost:3000/canal'

@Injectable({
  providedIn: 'root'
})
export class ApiNotifService {

  constructor(private http: HttpClient) { 

  }
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    };
  }
//GET
  getNotifications(): Observable<Notification[]> {
    return this.http.get<Notification[]>(`${apiUrl}`, httpOptions)
    .pipe(catchError(this.handleError('getNotifications', [])));
  }
  getNotificationById(id: String): Observable<Notification> {
    const url = `${apiUrl}/${id}`;
    return this.http.get<Notification>(url).pipe(
      catchError(this.handleError<Notification>(`getNotificationById=${id}`))
    );
  }
  //POST
  addNotification(notification: Notification): Observable<Notification> {
    return this.http.post<Notification>(apiUrl, notification, httpOptions).pipe(
      catchError(this.handleError<Notification>('addNotifications'))
    );
  }
  //PUT
  updateNotification(id: String, notification: Notification): Observable<any> {
    const url = `${apiUrl}/${id}`;
    return this.http.put(url, notification, httpOptions).pipe(
      catchError(this.handleError<any>('updateNotification'))
    );
  }
  //DELETE
  deleteNotificiation(id: String): Observable<Notification> {
    const url = `${apiUrl}/${id}`;
    return this.http.delete<Notification>(url, httpOptions)
      .pipe(
      catchError(this.handleError<Notification>('deleteNotification'))
    );
  }
   GetCountDOC() : Observable<any>
  {
    const url=UrlCanal+"/"
    return  this.http.get(url, httpOptions)
  }
  CountCanalmail() : Observable<any>
 {
  const url=UrlCanal+"/mail"
  return   this.http.get(url, httpOptions)
 }
  CountCanalsms() : Observable<any>
 {
  debugger
  const url=UrlCanal+"/sms"
  return  this.http.get(url, httpOptions)
 }
}
