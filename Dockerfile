FROM node:12.18.2-slim
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
COPY package.json package-lock.json ./
RUN npm install
RUN npm install -g @angular/cli@9.1.0
COPY . .
EXPOSE 4200
CMD ng serve --host 0.0.0.0